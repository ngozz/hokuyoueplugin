// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "HokuyoReadPluginGameModeBase.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOKUYOREADPLUGIN_HokuyoReadPluginGameModeBase_generated_h
#error "HokuyoReadPluginGameModeBase.generated.h already included, missing '#pragma once' in HokuyoReadPluginGameModeBase.h"
#endif
#define HOKUYOREADPLUGIN_HokuyoReadPluginGameModeBase_generated_h

#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_SPARSE_DATA
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_RPC_WRAPPERS
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_ACCESSORS
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHokuyoReadPluginGameModeBase(); \
	friend struct Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AHokuyoReadPluginGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/HokuyoReadPlugin"), NO_API) \
	DECLARE_SERIALIZER(AHokuyoReadPluginGameModeBase)


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAHokuyoReadPluginGameModeBase(); \
	friend struct Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AHokuyoReadPluginGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/HokuyoReadPlugin"), NO_API) \
	DECLARE_SERIALIZER(AHokuyoReadPluginGameModeBase)


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHokuyoReadPluginGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHokuyoReadPluginGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHokuyoReadPluginGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHokuyoReadPluginGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHokuyoReadPluginGameModeBase(AHokuyoReadPluginGameModeBase&&); \
	NO_API AHokuyoReadPluginGameModeBase(const AHokuyoReadPluginGameModeBase&); \
public: \
	NO_API virtual ~AHokuyoReadPluginGameModeBase();


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHokuyoReadPluginGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHokuyoReadPluginGameModeBase(AHokuyoReadPluginGameModeBase&&); \
	NO_API AHokuyoReadPluginGameModeBase(const AHokuyoReadPluginGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHokuyoReadPluginGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHokuyoReadPluginGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHokuyoReadPluginGameModeBase) \
	NO_API virtual ~AHokuyoReadPluginGameModeBase();


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_12_PROLOG
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_SPARSE_DATA \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_RPC_WRAPPERS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_ACCESSORS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_INCLASS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_SPARSE_DATA \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_ACCESSORS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOKUYOREADPLUGIN_API UClass* StaticClass<class AHokuyoReadPluginGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
