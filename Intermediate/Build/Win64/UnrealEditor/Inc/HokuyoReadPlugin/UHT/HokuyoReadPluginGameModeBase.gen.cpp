// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HokuyoReadPlugin/HokuyoReadPluginGameModeBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHokuyoReadPluginGameModeBase() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	HOKUYOREADPLUGIN_API UClass* Z_Construct_UClass_AHokuyoReadPluginGameModeBase();
	HOKUYOREADPLUGIN_API UClass* Z_Construct_UClass_AHokuyoReadPluginGameModeBase_NoRegister();
	UPackage* Z_Construct_UPackage__Script_HokuyoReadPlugin();
// End Cross Module References
	void AHokuyoReadPluginGameModeBase::StaticRegisterNativesAHokuyoReadPluginGameModeBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AHokuyoReadPluginGameModeBase);
	UClass* Z_Construct_UClass_AHokuyoReadPluginGameModeBase_NoRegister()
	{
		return AHokuyoReadPluginGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_HokuyoReadPlugin,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "HokuyoReadPluginGameModeBase.h" },
		{ "ModuleRelativePath", "HokuyoReadPluginGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AHokuyoReadPluginGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics::ClassParams = {
		&AHokuyoReadPluginGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AHokuyoReadPluginGameModeBase()
	{
		if (!Z_Registration_Info_UClass_AHokuyoReadPluginGameModeBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AHokuyoReadPluginGameModeBase.OuterSingleton, Z_Construct_UClass_AHokuyoReadPluginGameModeBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AHokuyoReadPluginGameModeBase.OuterSingleton;
	}
	template<> HOKUYOREADPLUGIN_API UClass* StaticClass<AHokuyoReadPluginGameModeBase>()
	{
		return AHokuyoReadPluginGameModeBase::StaticClass();
	}
	AHokuyoReadPluginGameModeBase::AHokuyoReadPluginGameModeBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHokuyoReadPluginGameModeBase);
	AHokuyoReadPluginGameModeBase::~AHokuyoReadPluginGameModeBase() {}
	struct Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AHokuyoReadPluginGameModeBase, AHokuyoReadPluginGameModeBase::StaticClass, TEXT("AHokuyoReadPluginGameModeBase"), &Z_Registration_Info_UClass_AHokuyoReadPluginGameModeBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AHokuyoReadPluginGameModeBase), 3715918205U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_681249627(TEXT("/Script/HokuyoReadPlugin"),
		Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Source_HokuyoReadPlugin_HokuyoReadPluginGameModeBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
