#include "SMenuWidget.h"

void SMenuWidget::Construct(const FArguments& InArgs)
{
	bCanSupportFocus = true;

	const FMargin ContentPadding = FMargin(500.0f, 300.0f);
	const FMargin ButtonPadding = FMargin(10.0f);

	FSlateFontInfo ButtonTextStyle = FCoreStyle::Get().GetFontStyle("EmbossedText");
	ButtonTextStyle.Size = 40.0f;

	FSlateFontInfo TitleTextStyle = ButtonTextStyle;
	TitleTextStyle.Size = 60.0f;

	ChildSlot
		[
			SNew(SOverlay)
				+ SOverlay::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					SNew(SImage)
						.ColorAndOpacity(FLinearColor(0.0f, 0.0f, 0.0f, 0.5f))
				]
				+ SOverlay::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				.Padding(500.0f, 300.0f)
				[
					SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						[
							SNew(STextBlock)
								.Text(FText::FromString("Hokuyo Settings"))
								.Font(TitleTextStyle)
								.Justification(ETextJustify::Center)
						]
						+ SVerticalBox::Slot()
						[
							SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.AutoWidth()
								[
									SNew(STextBlock) // Label
										.Text(FText::FromString("Epsilon: "))
										.Font(ButtonTextStyle)
								]
								+ SHorizontalBox::Slot()
								.FillWidth(1.f)
								[
									SNew(SBox)
										.WidthOverride(10) // Set the width to 200
										.HeightOverride(10) // Set the height to 30
										[
											SNew(SEditableTextBox) // Text input box
												.OnTextChanged(this, &SMenuWidget::OnTextChanged)
										]
								]
						]
				]
		];
}

void SMenuWidget::OnTextChanged(const FText& NewText)
{
	// Handle text change
}