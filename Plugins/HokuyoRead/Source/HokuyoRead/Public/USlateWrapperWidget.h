#pragma once

// This is a wrapper widget for the Slate widget (SMenuWidget) in order to use it in blueprints.
// The Slate widget is not directly usable in blueprints, so we need to wrap it in a UUserWidget.
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SMenuWidget.h"
#include "USlateWrapperWidget.generated.h"

UCLASS(BlueprintType)
class HOKUYOREAD_API USlateWrapperWidget : public UUserWidget
{
    GENERATED_BODY()

protected:
    virtual TSharedRef<SWidget> RebuildWidget() override;
};
