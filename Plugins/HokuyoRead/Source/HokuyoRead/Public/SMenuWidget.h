#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Widgets/Views/STableViewBase.h"
#include "Widgets/Views/STableRow.h"
#include "Widgets/Input/SEditableTextBox.h"
#include "Widgets/Text/STextBlock.h"

class HOKUYOREAD_API SMenuWidget : public SCompoundWidget
{
public:
    SLATE_BEGIN_ARGS(SMenuWidget)
        {}
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs);

private:
    TSharedPtr<SEditableTextBox> TextBox;
    void OnTextChanged(const FText& NewText);
};