// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "USlateWrapperWidget.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOKUYOREAD_USlateWrapperWidget_generated_h
#error "USlateWrapperWidget.generated.h already included, missing '#pragma once' in USlateWrapperWidget.h"
#endif
#define HOKUYOREAD_USlateWrapperWidget_generated_h

#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_SPARSE_DATA
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_RPC_WRAPPERS
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_ACCESSORS
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSlateWrapperWidget(); \
	friend struct Z_Construct_UClass_USlateWrapperWidget_Statics; \
public: \
	DECLARE_CLASS(USlateWrapperWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HokuyoRead"), NO_API) \
	DECLARE_SERIALIZER(USlateWrapperWidget)


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUSlateWrapperWidget(); \
	friend struct Z_Construct_UClass_USlateWrapperWidget_Statics; \
public: \
	DECLARE_CLASS(USlateWrapperWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HokuyoRead"), NO_API) \
	DECLARE_SERIALIZER(USlateWrapperWidget)


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USlateWrapperWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USlateWrapperWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USlateWrapperWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USlateWrapperWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USlateWrapperWidget(USlateWrapperWidget&&); \
	NO_API USlateWrapperWidget(const USlateWrapperWidget&); \
public: \
	NO_API virtual ~USlateWrapperWidget();


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USlateWrapperWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USlateWrapperWidget(USlateWrapperWidget&&); \
	NO_API USlateWrapperWidget(const USlateWrapperWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USlateWrapperWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USlateWrapperWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USlateWrapperWidget) \
	NO_API virtual ~USlateWrapperWidget();


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_10_PROLOG
#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_SPARSE_DATA \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_RPC_WRAPPERS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_ACCESSORS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_INCLASS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_SPARSE_DATA \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_ACCESSORS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_INCLASS_NO_PURE_DECLS \
	FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOKUYOREAD_API UClass* StaticClass<class USlateWrapperWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
