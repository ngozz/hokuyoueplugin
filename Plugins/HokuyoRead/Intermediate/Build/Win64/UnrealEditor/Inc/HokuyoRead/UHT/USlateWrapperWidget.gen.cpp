// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "../../Public/USlateWrapperWidget.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSlateWrapperWidget() {}
// Cross Module References
	HOKUYOREAD_API UClass* Z_Construct_UClass_USlateWrapperWidget();
	HOKUYOREAD_API UClass* Z_Construct_UClass_USlateWrapperWidget_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_HokuyoRead();
// End Cross Module References
	void USlateWrapperWidget::StaticRegisterNativesUSlateWrapperWidget()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USlateWrapperWidget);
	UClass* Z_Construct_UClass_USlateWrapperWidget_NoRegister()
	{
		return USlateWrapperWidget::StaticClass();
	}
	struct Z_Construct_UClass_USlateWrapperWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USlateWrapperWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_HokuyoRead,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlateWrapperWidget_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "USlateWrapperWidget.h" },
		{ "ModuleRelativePath", "Public/USlateWrapperWidget.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USlateWrapperWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USlateWrapperWidget>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USlateWrapperWidget_Statics::ClassParams = {
		&USlateWrapperWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USlateWrapperWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USlateWrapperWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USlateWrapperWidget()
	{
		if (!Z_Registration_Info_UClass_USlateWrapperWidget.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USlateWrapperWidget.OuterSingleton, Z_Construct_UClass_USlateWrapperWidget_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USlateWrapperWidget.OuterSingleton;
	}
	template<> HOKUYOREAD_API UClass* StaticClass<USlateWrapperWidget>()
	{
		return USlateWrapperWidget::StaticClass();
	}
	USlateWrapperWidget::USlateWrapperWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USlateWrapperWidget);
	USlateWrapperWidget::~USlateWrapperWidget() {}
	struct Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USlateWrapperWidget, USlateWrapperWidget::StaticClass, TEXT("USlateWrapperWidget"), &Z_Registration_Info_UClass_USlateWrapperWidget, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USlateWrapperWidget), 1368233752U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_2762273956(TEXT("/Script/HokuyoRead"),
		Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Unreal_Projects_Gitlab_Hokuyo_Plugin_Plugins_HokuyoRead_Source_HokuyoRead_Public_USlateWrapperWidget_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
