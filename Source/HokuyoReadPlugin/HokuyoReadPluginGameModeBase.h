// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HokuyoReadPluginGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOKUYOREADPLUGIN_API AHokuyoReadPluginGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
